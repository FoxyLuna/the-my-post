"""news URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))

"""
from django.urls import re_path, include
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth.views import login, logout
from post import views

from post.views import *

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    #url(r'^$', views.home, name='home'),
    re_path(r'^$', HomeList.as_view(), name='home'),
    re_path(r'^article/(?P<pk>[0-9]+)$', article_detail, name='article_detail'),
    re_path(r'^callback/$', CallbackView.as_view(), name='callback'),
    re_path(r'^thanks/$', ThanksView.as_view(), name='thanks'),
]
if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns() + static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)