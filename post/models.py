from django.db import models
from django.utils import timezone
from django.forms import ModelForm

class Callback(models.Model):
	subject = models.CharField(max_length=30)
	email = models.EmailField(blank=True)
	message = models.TextField()

	def __str__(self):
		return self.subject

	def publish(self):
		self.save()

	class Meta:
		ordering = ['subject']

class Author(models.Model):
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=40)
	email = models.EmailField(blank=True)

	def __str__(self):
		return '{0} {1}'.format(self.first_name, self.last_name)

class Article(models.Model):
	author = models.ForeignKey(Author, on_delete=models.CASCADE,)
	subject = models.CharField(max_length=50)
	post = models.TextField()
	publication_date = models.DateTimeField()
	image = models.ImageField(null=True, blank=True, upload_to='images/', verbose_name='Image',)

	class Meta:
		ordering = ['-publication_date']

	def __str__(self):
		return self.post

	def miniature(self):
		if self.image:
			return '<img src="{0}" width="70" />'.format(self.image.url)
		else:
			return '(none)'
	miniature.short_description = 'Image'
	miniature.allow_tags = True