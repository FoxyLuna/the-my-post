from django.contrib import admin
from post.models import Callback, Author, Article

class CallbackAdmin(admin.ModelAdmin):
	list_display = ('subject', 'email', 'message')

class AuthorAdmin(admin.ModelAdmin):
	list_display = ('first_name', 'last_name', 'email')

class ArticleAdmin(admin.ModelAdmin):
	list_display = ('author', 'subject', 'post', 'publication_date', 'image', 'miniature')
	ordering = ('author',)

admin.site.register(Callback, CallbackAdmin)
admin.site.register(Author, AuthorAdmin)
admin.site.register(Article, ArticleAdmin)
