from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import Article
from django.utils import timezone
from .forms import CallbackForm
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView, View

class HomeList(ListView):
	model = Article
	paginate_by = 5

	def get_context_data(self, **kwargs):
		context = super(HomeList, self).get_context_data(**kwargs)
		return context

class ThanksView(TemplateView):
	template_name = 'thanks.html'
	
	def get_context_data(self, **kwargs):
		context = super(ThanksView, self).get_context_data(**kwargs)
		return context

class CallbackView(View):
	form_class = CallbackForm
	template_name = 'contact.html'

	def get(self, request, *args, **kwargs):
		form = self.form_class()
		return render_to_response(self.template_name, {'form' : form})

	def post(self, request, *args, **kwargs):
		form = self.form_class(request.POST)
		if form.is_valid():
			callback = form.save(commit=True)
			request.session['comment'] = True
			return redirect('thanks')
		return render_to_response(self.template_name, {'form' : form})


def article_detail(request, pk):
	article = get_object_or_404(Article, pk=pk)
	return render_to_response('article_detail.html', {'article' : article})

"""
def callback(request):
	if request.method == 'POST':
		form = CallbackForm(request.POST)
		if form.is_valid():
			callback = form.save(commit=False)
			callback.save()
			request.session['comment'] = True
			return redirect('thanks')
	else:
		form = CallbackForm()
	return render_to_response('contact.html', {'form' : form})
"""
"""
class ArticleDetailView(TemplateView):
	pass
"""